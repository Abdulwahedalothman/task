import 'package:get_it/get_it.dart';
import 'catalog/application/catalog_facade_service.dart';
import 'catalog/infrastructure/data_sources/home_local_data_provider.dart';
import 'catalog/infrastructure/data_sources/home_remote_data_provider.dart';
import 'catalog/infrastructure/repositories/home_repository.dart';
import 'catalog/presentation/home/bloc/home_bloc.dart';
import 'common/platform/connectivity.dart';

final serviceLocator = GetIt.instance;

Future<void> init() async {
  // Catalog
  catalogDependencies();
}

Future<void> catalogDependencies() async {
  // Application Layer - facades
  serviceLocator.registerLazySingleton(() => CatalogFacadeService(
        homeRepository: serviceLocator(),
      ));

  // Infrastructure Layer
  // repositories
  serviceLocator.registerLazySingleton(
    () => HomeRepository(
      homeLocalDataProvider: serviceLocator(),
      homeRemoteDataProvider: serviceLocator(),
    ),
  );

  //data sources
  serviceLocator.registerLazySingleton(
    () => HomeLocalDataProvider(),
  );
  serviceLocator.registerLazySingleton(
    () => HomeRemoteDataProvider(
      dio: serviceLocator(),
    ),
  );

  // Presentation Layer - Blocs
  serviceLocator.registerFactory(
    () => HomeBloc(
      catalogService: serviceLocator(),
    ),
  );

  // Common and core
  serviceLocator.registerLazySingleton(
    () => getCacheNetworkObj(),
  );
  serviceLocator.registerLazySingleton(
    () => getNetworkObj(),
  );
}


