import 'package:flutter/material.dart';

import 'catalog/presentation/home/home.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case HomeUi.routeName:
        return MaterialPageRoute(
          builder: (_) => HomeUi(),
          settings: RouteSettings(name: HomeUi.routeName),
        );
      case HomeUi.baseRoute:
        return MaterialPageRoute(
          builder: (_) => HomeUi(),
          settings: RouteSettings(name: HomeUi.routeName),
        );
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return MaterialPageRoute(
            builder: (_) =>
                Scaffold(body: SafeArea(child: Text('Route Error'))));
    }
  }
}
