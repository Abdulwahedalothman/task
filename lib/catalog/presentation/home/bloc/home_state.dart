import 'package:equatable/equatable.dart';
import 'package:task/catalog/domain/entities/home/home_response.dart';

abstract class HomeState extends Equatable {
  const HomeState();
}

class InitialHomeState extends HomeState {
  @override
  List<Object> get props => [];
}

// ignore: must_be_immutable
class HomeLoadedState extends HomeState{
  HomeLoadedState(this.homeResponse);
  HomeResponse homeResponse;

  @override
  List<Object> get props => <dynamic>[homeResponse];

  @override
  String toString() {
    return 'HomeResponse : $homeResponse';
  }
}

class HomeLoadingState extends HomeState {
  @override
  List<Object> get props => [];
}

