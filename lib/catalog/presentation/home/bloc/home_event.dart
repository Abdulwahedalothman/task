import 'package:equatable/equatable.dart';

abstract class HomeEvent extends Equatable {
  const HomeEvent();
}

class FetchHomeEvent extends HomeEvent {
  FetchHomeEvent();

  @override
  String toString() {
    return 'FetchHomeEvent';
  }

  @override
  List<Object> get props => null;
}
