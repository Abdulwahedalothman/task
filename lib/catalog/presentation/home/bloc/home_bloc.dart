import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:task/catalog/application/catalog_facade_service.dart';
import 'package:task/catalog/domain/entities/common/response_type.dart';
import 'package:task/catalog/domain/entities/common/response_wrapper.dart';
import 'package:task/catalog/domain/entities/home/home_response.dart';

import 'home_event.dart';
import 'home_state.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final CatalogFacadeService catalogService;
  HomeBloc({this.catalogService}) : super(InitialHomeState());

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is FetchHomeEvent) {
      yield HomeLoadingState();
      yield* _mapFetchHomeEventToState();
    }
  }

  Stream<HomeLoadedState> _mapFetchHomeEventToState() async* {
    final ResponseWrapper<HomeResponse> responseWrapper =
        await catalogService.fetchHome();
    switch (responseWrapper.responseType) {
      case ResponseType.SUCCESS:
        yield HomeLoadedState(responseWrapper.data);
        break;
      case ResponseType.NETWORK_ERROR:
        break;
      case ResponseType.VALIDATION_ERROR:
        break;
      case ResponseType.CLIENT_ERROR:
        break;
      case ResponseType.SERVER_ERROR:
        break;
    }
  }
}
