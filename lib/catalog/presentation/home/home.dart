import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:task/catalog/domain/entities/home/home_response.dart';
import 'package:task/common/config/app_colors.dart';
import 'package:task/common/widgets/product_card.dart';

import '../../../injections.dart';
import 'bloc/home_bloc.dart';
import 'bloc/home_event.dart';
import 'bloc/home_state.dart';

class HomeUi extends StatefulWidget {
  static const routeName = '/Home';
  static const baseRoute = '/';

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return HomeUiViewState();
  }
}

HomeResponse homeResponse;
HomeBloc homeBloc;

class HomeUiViewState extends State<HomeUi> with TickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
    homeBloc = serviceLocator<HomeBloc>();
    homeBloc.add(FetchHomeEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: AppColors.red,
        title: Text('COLORS'),
        elevation: 0,
        centerTitle: true,
      ),
      body: Padding(
        padding: EdgeInsetsDirectional.only(start: 12, end: 12),
        child: BlocProvider<HomeBloc>(
          create: (BuildContext context) => serviceLocator<HomeBloc>(),
          child: BlocListener<HomeBloc, HomeState>(
            cubit: homeBloc,
            listener: (context, state) async {
              if (state is HomeLoadedState) {
                homeResponse = state.homeResponse;
              }
            },
            child: BlocBuilder<HomeBloc, HomeState>(
              cubit: homeBloc,
              builder: (BuildContext context, HomeState state) {
                if (state is HomeLoadedState) {
                  return ListView.builder(
                      scrollDirection: Axis.vertical,
                      itemCount: homeResponse.data.length,
                      itemBuilder: (context, index) {
                        return ProductCard(
                          color: homeResponse.data[index].color,
                          name: homeResponse.data[index].name,
                          value: homeResponse.data[index].pantoneValue,
                          year: homeResponse.data[index].year.toString(),
                        );
                      });
                }
                return Center(
                  child: CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(AppColors.red),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
