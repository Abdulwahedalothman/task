import 'package:dio/dio.dart';
import 'package:meta/meta.dart';
import 'package:task/catalog/domain/entities/common/response_wrapper.dart';
import 'package:task/catalog/domain/entities/home/home_response.dart';
import 'package:task/catalog/domain/interfaces/home_interface.dart';
import 'package:task/catalog/infrastructure/data_sources/home_local_data_provider.dart';
import 'package:task/catalog/infrastructure/data_sources/home_remote_data_provider.dart';
import 'package:task/catalog/domain/entities/common/response_type.dart'
    as ResType;

class HomeRepository implements HomeInterface {
  HomeRepository({
    @required this.homeLocalDataProvider,
    @required this.homeRemoteDataProvider,
  });

  final HomeLocalDataProvider homeLocalDataProvider;
  final HomeRemoteDataProvider homeRemoteDataProvider;

  @override
  Future<ResponseWrapper<HomeResponse>> fetchHome() async {
    try {
      Response response = await homeRemoteDataProvider.fetchHome();
      if (response.statusCode < 400 && response.statusCode > 199) {
        int code = response.statusCode;
        if (code == 200) {
          var res = ResponseWrapper<HomeResponse>();
          res.responseType = ResType.ResponseType.SUCCESS;
          res.data = HomeResponse.fromJson(response.data);
          return res;
        } else {
          var res = ResponseWrapper<HomeResponse>();
          res.responseType = ResType.ResponseType.SERVER_ERROR;
          return res;
        }
      } else if (response.statusCode > 399 && response.statusCode < 500) {
        var res = ResponseWrapper<HomeResponse>();
        res.responseType = ResType.ResponseType.SERVER_ERROR;
        return res;
      } else {
        var res = ResponseWrapper<HomeResponse>();
        res.responseType = ResType.ResponseType.SERVER_ERROR;
        return res;
      }
    } on DioError catch (e) {
      if (e.response != null) {
        if (e.response.statusCode == 200) {
          int code = e.response.data["code"];
          if (code == 200) {
            var res = ResponseWrapper<HomeResponse>();
            res.responseType = ResType.ResponseType.SUCCESS;
            res.data = HomeResponse.fromJson(e.response.data);
            return res;
          } else {
            var res = ResponseWrapper<HomeResponse>();
            res.responseType = ResType.ResponseType.SERVER_ERROR;
            return res;
          }
        } else if (e.response.statusCode > 399 && e.response.statusCode < 500) {
          var res = ResponseWrapper<HomeResponse>();
          res.responseType = ResType.ResponseType.SERVER_ERROR;
          return res;
        } else {
          var res = ResponseWrapper<HomeResponse>();
          res.responseType = ResType.ResponseType.SERVER_ERROR;
          return res;
        }
      } else {
        var res = ResponseWrapper<HomeResponse>();
        res.responseType = ResType.ResponseType.SERVER_ERROR;
        return res;
      }
    } catch (e) {
      var res = ResponseWrapper<HomeResponse>();
      res.responseType = ResType.ResponseType.CLIENT_ERROR;
      return res;
    }
  }
}
