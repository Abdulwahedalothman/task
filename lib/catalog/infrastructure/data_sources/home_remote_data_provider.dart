import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:task/common/config/config.dart';

class HomeRemoteDataProvider {
  HomeRemoteDataProvider({@required this.dio});

  final Dio dio;
  Future<dynamic> fetchHome() async {
    Response response = await dio.get(
      Urls.PRODUCTS,
    );
    return response;
  }
}
