import 'package:task/catalog/domain/entities/common/response_wrapper.dart';
import 'package:task/catalog/domain/entities/home/home_response.dart';
import 'package:task/catalog/infrastructure/repositories/home_repository.dart';

class CatalogFacadeService {
  const CatalogFacadeService({
    this.homeRepository,
  });

  final HomeRepository homeRepository;

  Future<ResponseWrapper<HomeResponse>> fetchHome() async {
    return homeRepository.fetchHome();
  }
}
