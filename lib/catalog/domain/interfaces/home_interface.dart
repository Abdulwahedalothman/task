import 'package:task/catalog/domain/entities/common/response_wrapper.dart';
import 'package:task/catalog/domain/entities/home/home_response.dart';

abstract class HomeInterface {
  Future<ResponseWrapper<HomeResponse>> fetchHome();
}
