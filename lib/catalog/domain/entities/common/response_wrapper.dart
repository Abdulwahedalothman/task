import 'package:task/catalog/domain/entities/common/response_type.dart';
import 'package:task/catalog/domain/entities/common/validation_error_parent.dart';

class ResponseWrapper<T> {
  T data;
  ResponseType responseType;
  ValidationErrorParent validationErrorParent;
  String message;
}
