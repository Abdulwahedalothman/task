import 'package:equatable/equatable.dart';

class LocalException extends Equatable {
  @override
  List<Object> get props => null;

  call(String message) {
    return message;
  }

  @override
  bool get stringify => null;
}
