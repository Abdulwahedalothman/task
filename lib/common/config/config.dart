class Urls {
  static const String BASE_URL = "https://reqres.in/api/";
  static const String PRODUCTS = "products";
}

class PrefsKeys {
  static const String TOKEN = "TOKEN";
}
