import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:task/common/config/config.dart';

import '../../injections.dart';

DioCacheManager getCacheNetworkObj() {
  return DioCacheManager(CacheConfig(baseUrl: Urls.BASE_URL));
}

Dio getNetworkObj() {
  BaseOptions options = new BaseOptions(
    baseUrl: Urls.BASE_URL,
    connectTimeout: 60000,
    receiveTimeout: 60000,
  );
  Dio dio = new Dio();
  dio.options.baseUrl = Urls.BASE_URL;
  DioCacheManager dioCache = serviceLocator<DioCacheManager>();
  dio.interceptors.add(dioCache.interceptor);
  dio.interceptors
      .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.get(PrefsKeys.TOKEN);
    if (token != null) if (token.isNotEmpty)
      options.headers["Authorization"] = "bearer " + token;
    return options;
  }, onResponse: (Response r) async {
    if (r.statusCode == 401) {
      dio.interceptors.requestLock.lock();
      Dio tokenDio = new Dio(options);
      tokenDio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String token = prefs.get(PrefsKeys.TOKEN);
        if (token != null) if (token.isNotEmpty)
          options.headers["Authorization"] = "bearer " + token;
        return options;
      }));
      dio.interceptors.requestLock.unlock();
    }
    return r;
  }, onError: (DioError r) async {
    if (r.response.statusCode == 401) {
      dio.interceptors.requestLock.lock();
      Dio tokenDio = new Dio(options);
      tokenDio.interceptors
          .add(InterceptorsWrapper(onRequest: (RequestOptions options) async {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        String token = prefs.get(PrefsKeys.TOKEN);
        if (token != null) if (token.isNotEmpty)
          options.headers["Authorization"] = "bearer " + token;
        return options;
      }));
      dio.interceptors.requestLock.unlock();
    }
    return r;
  }));
  return dio;
}