import 'package:flutter/material.dart';

class ProductCard extends StatelessWidget {
  final String name;
  final String year;
  final String value;
  final String color;

  const ProductCard({Key key, this.name, this.year, this.value, this.color})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 130,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: hexToColor(color),
          borderRadius: BorderRadius.all(
            Radius.circular(18),
          ),
        ),
        child: Center(
            child: Padding(
          padding: const EdgeInsets.all(4.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Name :  '),
                    Text(name),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Year :  '),
                    Text(year),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text('Value :  '),
                    Text(value),
                  ],
                ),
              ),
            ],
          ),
        )),
      ),
    );
  }

  hexToColor(String color) {
    return new Color(int.parse(color.substring(1, 7), radix: 16) + 0xFF000000);
  }
}
